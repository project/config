<?php

/**
 * @file
 * Contains a ConfigTestBase
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\config\Tests;

/**
 * Class ConfigTestBase
 * @package Drupal\config\Tests\File
 */
class ConfigTestBase extends \PHPUnit_Framework_TestCase {

}
