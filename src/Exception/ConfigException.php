<?php

/**
 * @file
 * Contains a ConfigException
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\config\Exception;

use Exception;

/**
 * Class ConfigException
 * @package Drupal\config
 */
class ConfigException extends \Exception {

}
